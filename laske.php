<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h3>Nettopalkka</h3>
    <?php
    $brutto = filter_input(INPUT_POST,'brutto',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $ennakko = filter_input(INPUT_POST,'ennakko',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $tyoelake = filter_input(INPUT_POST,'tyoelake',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    $tyottomyys = filter_input(INPUT_POST,'tyottomyys',FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
    printf("<p>Bruttopalkka: %d €</p>",$brutto);
    $ennakkolasku = $brutto * ($ennakko / 100);
    printf("<p>Ennakkopidätys: %.2f €</p>",$ennakkolasku);
    $tyoelakelasku = $brutto * ($tyoelake / 100);
    printf("<p>Työeläkemaksu: %.2f €</p>",$tyoelakelasku);
    $tyottomyyslasku = $brutto * ($tyottomyys / 100);
    printf("<p>Työttömyysvakuutusmaksu: %.2f €</p>",$tyottomyyslasku);
    $nettopalkka = $brutto - $ennakkolasku - $tyoelakelasku - $tyottomyyslasku;
    printf("<p>Nettopalkka on %.2f €</p>",$nettopalkka);
    ?>    
    <a href="index.html">Laske uudestaan</a>
</body>
</html>